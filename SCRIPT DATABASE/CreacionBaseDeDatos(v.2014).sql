CREATE DATABASE [Challenge_NBM]
GO
USE [Challenge_NBM]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuarios](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Apellido] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](150) NULL,
	[Password] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Usuarios] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Usuarios] ON 

GO
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Apellido], [Email], [Password]) VALUES (1, N'Fernando', N'Gomez', N'nn@nn.com', N'ew6r89ewr8ewr8f44f')
GO
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Apellido], [Email], [Password]) VALUES (2, N'Habier', N'Pozky', N'pos@hotm.com', N'ew56r465ew4r65ew46ewr4')
GO
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Apellido], [Email], [Password]) VALUES (4, N'Luis', N'Torres', N'torrea@gmaio.com', N'sdadadwed5846as4d')
GO
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Apellido], [Email], [Password]) VALUES (5, N'Maria', N'Lauvinch', N'mariaLou@magil.com', N'5d4f6s5df4sd4f6s4fsdf46')
GO
INSERT [dbo].[Usuarios] ([Id], [Nombre], [Apellido], [Email], [Password]) VALUES (6, N'Rita', N'Lucero', N'rlucero@gmaiil.com', N'5dsf456s4f65s4fd65sd4f6')
GO
SET IDENTITY_INSERT [dbo].[Usuarios] OFF
GO
